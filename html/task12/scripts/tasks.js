function Task(index, name, speed) {
    this.getIndex = function() { return index; }
    this.Name = function() {
        return "#".concat(index).concat(" ").concat(name);
    }
    this.getSpeed = function() { return speed; }
    var progress = 0;

    this.DoStep = function() {
        progress += 0.2 * this.getSpeed();
        console.log(progress);
    }

    this.GetProgress = function() {
        return progress;
    }
}

function fastTask(index) {
    return new Task(index, "Fast Task", 20);
}

function mediumTask(index) {
    return new Task(index, "Medium Task", 15);
}

function slowTask(index) {
    return new Task(index, "Slow Task", 10);
}

window.Tasks = { "Fast Task": fastTask, "Medium Task": mediumTask, "Slow Task": slowTask };