﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Input;
using GaussBlur_task04.Helpers;
using GaussBlur_task04.ViewModels;
using Microsoft.Win32;

namespace GaussBlur_task04.Commands
{
    public class LoadImageCommand : ICommand
    {
        private MainViewModel viewModel;

        public LoadImageCommand(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.viewModel.PropertyChanged += this.OnViewModelChangedState;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return this.viewModel.IsNotRunProcessing;
        }

        public void Execute(object parameter)
        {
            this.viewModel.IsNotRunProcessing = false;
            OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image file(*.png)|*.png";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                this.viewModel.ImagePath = openFileDialog.FileName;
                Bitmap bitmap = new Bitmap(Bitmap.FromFile(openFileDialog.FileName, true));
                this.viewModel.ImagePixels = bitmap.ToPixels();
                this.viewModel.OriginBitmapImage = bitmap.ToBlackAndWhite().ToImageSource();
                this.viewModel.BlurredBitmapImage = null;
            }

            this.viewModel.IsNotRunProcessing = true;
        }

#pragma warning disable Nix09 // The parameter is never used
        private void OnViewModelChangedState(object sender, PropertyChangedEventArgs e)
#pragma warning restore Nix09 // The parameter is never used
        {
            if (e.PropertyName == nameof(this.viewModel.IsNotRunProcessing))
            {
                this.CanExecuteChanged?.Invoke(this, e);
            }
        }
    }
}