﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Serialization_task05
{
    public class Program
    {
        private static string filePath = "order.xml";

        public static void Main()
        {
            Order order = new Order(2, 5);
            XmlSerializer formatter = new XmlSerializer(typeof(Order));
            RunSerialization(order, formatter);
            Order newOrder = RunDeserialization(formatter);
            ShowOrder(newOrder);
            Console.ReadLine();
        }

        private static void RunSerialization(Order order, XmlSerializer formatter)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, order);

                Console.WriteLine("Объект сериализован");
            }
        }

        private static Order RunDeserialization(XmlSerializer formatter)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
              return (Order)formatter.Deserialize(fs);
            }
        }

        private static void ShowOrder(Order order)
        {
            Console.WriteLine("+-------+-------+-------------+");
            Console.WriteLine("| Price | Count | Total price |");
            Console.WriteLine("+-------+-------+-------------+");
            Console.WriteLine("| {0,5} | {1,5} | {2,10}  |", order.Count, order.Price, order.TotalPrice);
            Console.WriteLine("+-------+-------+-------------+");
        }
    }
}