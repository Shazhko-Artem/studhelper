﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02
{
    class Program
    {
        static void Main(string[] args)
        {
            Commands.CommandManager manager = new Commands.CommandManager(
                new Commands.CommandProcessor(
                    new Commands.ICommand[] {
                        new Commands.ShowFileTreeCommand(),
                        new Commands.ArchivingCommand("Images.zip",new []{ ".png" ,".jpeg",".jpg",".svg"})
                    }));

            manager.Start();
        }
    }
}
