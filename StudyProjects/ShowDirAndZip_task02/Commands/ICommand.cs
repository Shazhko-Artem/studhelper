﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowDirAndZip_task02.Commands
{
    public interface ICommand
    {
        int Number { get; }

        string DisplayName { get; }

        void Execute();
    }
}
