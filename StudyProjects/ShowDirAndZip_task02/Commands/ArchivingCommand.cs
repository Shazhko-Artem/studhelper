﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace ShowDirAndZip_task02.Commands
{
    public class ArchivingCommand : ICommand
    {
        private readonly string archiveName;
        private readonly IEnumerable<string> formatFilter;

        public ArchivingCommand(string archiveName,IEnumerable<string> formatFilter)
        {
            this.archiveName = archiveName;
            this.formatFilter = formatFilter;
        }

        public ArchivingCommand(string archiveName) :this(archiveName,new List<string>())
        { }

        public int Number => 2;

        public string DisplayName => "Archiving files";

        public void Execute()
        {
            string pathFiles = GetDirectoryPath();
            string arhivePath = GetDirectoryPath();
            CreateZip(pathFiles, arhivePath, archiveName);
        }

        private string GetDirectoryPath()
        {
            Console.Write("[directory path] -> ");
            string path = string.Empty;

            while (string.IsNullOrEmpty(path))
            {
                path = Console.ReadLine();
                if (!Directory.Exists(path))
                {
                    Console.WriteLine("Directory is not exists");
                    path = string.Empty;
                }
            }

            return path;
        }
        private void CreateZip(string directoryToCompress, string directoryForArchive, string archiveName)
        {

            var files = Directory.GetFiles(directoryToCompress, "*.*", SearchOption.AllDirectories)
       .Where(file => formatFilter.Any(ff=>file.EndsWith(ff)))
       .ToList();

            using (FileStream zipToOpen = new FileStream(Path.Combine(directoryForArchive, archiveName), FileMode.Create))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    foreach (var fileName in files)
                    {
                        archive.CreateEntryFromFile(fileName, fileName.Substring(fileName.LastIndexOf('\\') + 1));
                    }
                }
            }
        }
    }
}
