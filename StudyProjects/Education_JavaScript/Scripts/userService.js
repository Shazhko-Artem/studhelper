﻿var userService = {
    getUsers: function(withError, callback) {
        $.ajax({
            url: 'Home/GetData',
            data: { withError: withError },
            type: 'GET',
            success: function (data) {
                callback("success", data);
            },
            error: function (data) {
                callback("error", data);
            }
        });
    }
}