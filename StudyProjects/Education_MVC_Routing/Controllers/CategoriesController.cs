﻿using System.Web.Mvc;
using Education_MVC_Routing.ViewModels;

namespace Education_MVC_Routing.Controllers
{
    public class CategoriesController : Controller
    {
        public ActionResult List()
        {
            return View();
        }

        public ActionResult Category(string categoryName)
        {
            return View(new CategoryViewModel(categoryName));
        }

        public ActionResult SubCategories(string categoryName)
        {
            return View(new CategoryViewModel(categoryName));
        }

        public ActionResult SubCategory(string categoryName, string subCategoryName)
        {
            return View(new CategoryViewModel(categoryName, subCategoryName));
        }
    }
}
