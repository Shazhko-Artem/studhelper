﻿using System;
using ReflectionMap_task03.Domain;
using ReflectionMap_task03.SimpleMapper;
using ReflectionMap_task03.ViewModel;

namespace ReflectionMap_task03
{
    public class Program
    {
        public static void Main()
        {
            User user = new User()
            {
                Name = "User 1",
                Age = 5,
                Address = "Kiev"
            };

            IMapper mapper = new SimpleMapper.SimpleMapper();

            var userModel = mapper.Map<UserViewModel>(user);

            Console.WriteLine(userModel);
        }
    }
}