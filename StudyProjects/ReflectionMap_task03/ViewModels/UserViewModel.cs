﻿namespace ReflectionMap_task03.ViewModel
{
    public class UserViewModel
    {
        public int Age { get; set; }

        public string Name { get; set; }
    }
}