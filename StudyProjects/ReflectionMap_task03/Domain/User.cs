﻿namespace ReflectionMap_task03.Domain
{
    public class User
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string Address { get; set; }
    }
}