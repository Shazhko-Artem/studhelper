﻿namespace ReflectionMap_task03.SimpleMapper
{
    public interface IMapper
    {
        TOut Map<TOut>(object source);
    }
}