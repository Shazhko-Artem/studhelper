﻿namespace ReflectionMap_task03.ViewModel
{
    public class UserViewModel
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}